# Overview

Nodejs module - adapted version of [`cOMPLEXdATAtYPE.js` library](https://bitbucket.org/workslon/ontojs/src/8367a54d7dbdc4330a750dfdfbbd9a3e27e17867/src/complexdatatype.js) written by Prof. Gerd Wagner, BTU Cottbus

You can find the source code [here](https://bitbucket.org/workslon/complexdatatype/src/02c5db71ff9612f98203004ec60ff7c491a31aa5/index.js)

# Install

## ssh

```
npm install git+ssh://git@bitbucket.org/workslon/complexdatatype.git --save
```

## https

```
npm install https://git@bitbucket.org/workslon/complexdatatype.git --save
```

# Usage

## CommonJS

```javascript
var cOMPLEXdATAtYPE = require('cOMPLEXdATAtYPE');

// ...
```

## ES6 Modules

```javascript
import cOMPLEXdATAtYPE from 'cOMPLEXdATAtYPE';

// ...
```